//#define SYNTH -1 // Debug
//#define SYNTH 0 // Sonant
#define SYNTH 1 // Soundbox

#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>

#define WIN32_LEAN_AND_MEAN
#define WIN32_EXTRA_LEAN

#include <windows.h>
#include <mmsystem.h>
#pragma comment(lib,"Winmm.lib")

static HWAVEOUT wave_handle = 0;
static WAVEFORMATEX wave_format;
static WAVEHDR wave_header[2];

static int wave_pending = 0;
static int wave_currentBuf = 0;

static void (CALLBACK waveOutProc)(HWAVEOUT m_hWO, UINT uMsg, DWORD dwInstance, DWORD dwParam1, DWORD dwParam2)
{
    if (uMsg == MM_WOM_DONE)
    {
        wave_pending = wave_pending - 1;
    }
}

void playMusic(void* buffer, unsigned int bufferSize, unsigned int WAVE_CHAN=2, unsigned int WAVE_SPS=44100, unsigned int WAVE_BITS=16, int WAVE_ALIGN=-1)
{
    // block until channel is free
    while (wave_pending >= 2) { Sleep(1); }

    // init sound device
    if (wave_handle == 0)
    {
        if (WAVE_ALIGN == -1)
            WAVE_ALIGN = WAVE_CHAN * WAVE_BITS / 8;

        WAVEFORMATEX pwave_format = {
            WAVE_FORMAT_PCM, //wFormatTag
            WAVE_CHAN, // nChannels
            WAVE_SPS, // nSamplesPerSec
            WAVE_ALIGN * WAVE_SPS, // nAvgBytesPerSec
            WAVE_ALIGN, // nBlockAlign
            WAVE_BITS // wBitsPerSample
        };

        memcpy(&wave_format, &pwave_format, sizeof(pwave_format));
        MMRESULT res = waveOutOpen(&wave_handle, WAVE_MAPPER, &wave_format, (DWORD_PTR)& waveOutProc, 0, CALLBACK_FUNCTION);
        wave_header[0].lpData = 0;
        wave_header[1].lpData = 0;
    }

    WAVEHDR pwave_header = {
        (char*)buffer, // lpData
        bufferSize, // dwBufferLength
        0, // dwBytesRecorded
        wave_currentBuf, // dwUser
        0, // dwFlags
        0, // dwLoops
        0, // lpNext
        0 // reserved
    };


    memcpy(wave_header+wave_currentBuf, &pwave_header, sizeof(pwave_header));
    if(wave_header[wave_currentBuf].lpData != 0)
        MMRESULT res = waveOutUnprepareHeader(wave_handle, wave_header + wave_currentBuf, sizeof(WAVEHDR));
    MMRESULT res2 = waveOutPrepareHeader(wave_handle, wave_header+ wave_currentBuf, sizeof(WAVEHDR));
    MMRESULT res3 = waveOutWrite(wave_handle, wave_header+ wave_currentBuf, sizeof(WAVEHDR));
    wave_currentBuf++;
    wave_pending++;
    if (wave_currentBuf == 2)
        wave_currentBuf = 0;
}


void sleepeternal()
{
    printf("enjoy..\n");
    ULONGLONG t = GetTickCount64();
    while (1) {
        printf("%d\n", (int)(GetTickCount64() - t));
        Sleep(1000);
    }
}

// Debug synth
#if SYNTH == -1

#include <math.h>

int main(int argc, char** argv)
{
    short sine[44100 * 2];
    for (int s = 0; s < sizeof(sine)/4; s++)
    {
        float t = (float)s / 44100.0f;
        short val = sinf(t * 440.0f * 3.1415f * 2.0f) * 30000;
        int b = s << 1;
        sine[b + 0] = val;
        sine[b + 1] = val;
    }
    playMusic(sine, sizeof(sine));
    while (wave_pending > 0)
    {
        Sleep(100);
    }
    return 0;
}
#endif

// Sonant
#if SYNTH == 0

extern "C" {
#include "sonant.h"
#include "music.h"
}

int main(int argc, char** argv)
{
    printf("making musak..\n");
    unsigned int len;
    short *buffer = sonant_init(&songdata, _4K_SONANT_ENDPATTERN_, _4K_SONANT_ROWLEN_, &len);
    playMusic(buffer, len);

    sleepeternal();
    return 0;
}
#endif

// Soundbox
#if SYNTH == 1

extern "C" {
#include "soundbox.h"
}

#include "json.hpp"
using json = nlohmann::json;

#include <chrono>

int main(int argc, char** argv)
{
    printf("making music..\n");

    // parse song
    char song[100000];
    const char* ssong = "song.json";
    if (argc == 2)
        ssong = argv[1];
    FILE* f = fopen(ssong, "rb");
    size_t sz = fread(song, 1, sizeof(song), f);
    song[sz] = 0; // exploitable..
    fclose(f);
    auto songJson = json::parse(song);

    // build song struct
    sndbox_song sng;
    memset(&sng, 0, sizeof(sng));
    sng.numChannels = songJson["numChannels"];
    sng.rowLen = songJson["rowLen"];
    sng.patternLen = songJson["patternLen"];
    sng.endPattern = songJson["endPattern"];

    // parse songdata
    int instrumentID = 0;

    for (auto instrument : songJson["songData"])
    {
        auto& instrumentOut = sng.songData[instrumentID++];
        int id = 0, subid = 0;
        // parse instrument data 
        id = 0; for (auto v : instrument["p"]) { if (v.is_number()) instrumentOut.p[id] = (int)v; ++id; }

        // parse pattern data
        id = 0; for (auto v : instrument["i"]) { if (v.is_number()) instrumentOut.i[id] = v; ++id; }

        // parse column data
        id = 0; for (auto column : instrument["c"])
        {
            auto& columnOut = instrumentOut.c[id++];

            subid = 0; for (auto v : column["n"]) { if (v.is_number()) columnOut.n[subid] = (int)v; ++subid; }
            subid = 0; for (auto v : column["f"]) { if (v.is_number()) columnOut.f[subid] = (int)v; ++subid; }
        }
    }

    sng.ident[0] = 'S';
    sng.ident[1] = 'N';
    sng.ident[2] = 'D';
    sng.ident[3] = 'b';

    f = fopen("song.bin", "wb");
    fwrite(&sng, 1, sizeof(sndbox_song), f);
    fclose(f);
    
    //short* buffer = sonant_init(&songdata, _4K_SONANT_ENDPATTERN_, _4K_SONANT_ROWLEN_, &len);
    float status = 0.0f;

    sndbox_chnstate state[32];
    sndbox_config config;
    //float freq = 44100.f;
    //float freq = 22050.f;
    float freq = 11025.f;
    config.channels = 2;
    config.freqDivider = 44100.f / freq;

    for(int i=0; i<sng.numChannels; i++)
        sndbox_init_generate_chn(&sng, &config, state+i, i);

    short* waveBuffer[3];
    int activeWaveBuffer = 0;
    for(int i=0; i<3; i++)
        waveBuffer[i] = (short*)malloc(sizeof(short) * 20000);

    int p = -1;
    while(true)
    {
        int done = 0;

        // generate audio
        auto start = std::chrono::high_resolution_clock::now();
        float mixbuf[20000];
        int samples = 0;
        memset(&mixbuf, 0, sizeof(float)*sng.rowLen*config.channels);
        for (int i = 0; i < sng.numChannels; i++)
            done += sndbox_generate(state + i, mixbuf, &samples);
        auto end = std::chrono::high_resolution_clock::now();
        
        // convert to wave
        if (samples > 0)
        {
            unsigned int len = sndbox_createWave(waveBuffer[activeWaveBuffer], 0, mixbuf, samples);
            playMusic(waveBuffer[activeWaveBuffer], len, config.channels, (int)freq);
            ++activeWaveBuffer;
            if (activeWaveBuffer > 2)
                activeWaveBuffer = 0;
        }


        // status
        if (p != state->p)
        {
            printf("%lld us - smp: %d - %d/%d\n", std::chrono::duration_cast<std::chrono::microseconds>(end-start).count(), samples, state->p, sng.endPattern);
            p = state->p;
        }
        if (done == sng.numChannels)
            break;
    }

    // wait until all audio has stopped and cleanup
    while (wave_pending > 0) { Sleep(10); }
    for(int i=0; i<3; i++)
        free(waveBuffer[i]);

    return 0;
}
#endif