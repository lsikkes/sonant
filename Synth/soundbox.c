/* -*- mode: javascript; tab-width: 4; indent-tabs-mode: nil; -*-
*
* Copyright (c) 2011-2013 Marcus Geelnard
*
* This software is provided 'as-is', without any express or implied
* warranty. In no event will the authors be held liable for any damages
* arising from the use of this software.
*
* Permission is granted to anyone to use this software for any purpose,
* including commercial applications, and to alter it and redistribute it
* freely, subject to the following restrictions:
*
* 1. The origin of this software must not be misrepresented; you must not
*    claim that you wrote the original software. If you use this software
*    in a product, an acknowledgment in the product documentation would be
*    appreciated but is not required.
*
* 2. Altered source versions must be plainly marked as such, and must not be
*    misrepresented as being the original software.
*
* 3. This notice may not be removed or altered from any source
*    distribution.
*
*/

#include <string.h>
#include <stdlib.h>
#include <math.h>

#include "soundbox.h"

    //--------------------------------------------------------------------------
    // Private methods
    //--------------------------------------------------------------------------

    // Oscillators
    static float osc_sin (float value) {
        return sinf(value * 6.283184);
    }

    static float osc_saw (float value) {
        return 2.0f * fmodf(value, 1.0f) - 1.0f;
    }

    static float osc_square(float value) {
        return fmodf(value, 1.f) < 0.5f ? 1.f : -1.f;
    };

    static float osc_tri(float value) {
        float v2 = fmodf(value, 1.0f) * 4.f;
        if (v2 < 2.f) return v2 - 1.f;
        return 3.f - v2;
    };

    // Array of oscillator functions
    static float (*mOscillators[])(float)  = {
        osc_sin,
        osc_square,
        osc_saw,
        osc_tri
    };

    static float getnotefreq(float n) {
        // 174.61.. / 44100 = 0.003959503758 (F3)
        return 0.003959503758f * powf(2, (n - 128) / 12);
    };

    static unsigned int randseed = 1;
    static float randfloat4k() {
        return (float)((randseed *= 0x15a4e35) % 255) / 255.0f;
    }

    unsigned int sndbox_getNoteSize(sndbox_instrument* instr)
    {
        int attack = (instr->i[10] * instr->i[10]) * 4,
            sustain = (instr->i[11] * instr->i[11]) * 4,
            release = (instr->i[12] * instr->i[12]) * 4;
        return attack + sustain + release;
    }

    void sndbox_createNote(float* noteBuf, sndbox_instrument* instr, float n, float rowLen) {
        float (*osc1)(float) = mOscillators[(int)instr->i[0]],
            (*osc2)(float) = mOscillators[(int)instr->i[4]];

        float attack = (instr->i[10] * instr->i[10]) * 4.0f,
            sustain = (instr->i[11] * instr->i[11]) * 4.0f,
            release = (instr->i[12] * instr->i[12]) * 4.0f;

        float    o1vol = instr->i[1],
            o1xenv = instr->i[3],
            o2vol = instr->i[5],
            o2xenv = instr->i[8],
            noiseVol = instr->i[9],
            releaseInv = 1.f / release,
            arpInterval = rowLen * powf(2.f, 2.f - instr->i[14]);
        int arp = (int)instr->i[13];



        // Re-trig oscillators
        float c1 = 0.f, c2 = 0.f;

        // Local variables.
        int j = 0, j2 = 0;
        float e = 0.0f, t = 0.0f;
        float rsample = 0.f, o1t = 0.f, o2t = 0.f;

        // Generate one note (attack + sustain + release)
        for (j = 0, j2 = 0; j < attack + sustain + release; j++, j2++) {
            if (j2 >= 0) {
                // Switch arpeggio note.
                arp = (arp >> 8) | ((arp & 255) << 4);
                j2 -= arpInterval;

                // Calculate note frequencies for the oscillators
                o1t = getnotefreq(n + (arp & 15) + instr->i[2] - 128.f);
                o2t = getnotefreq(n + (arp & 15) + instr->i[6] - 128.f) * (1.f + 0.0008 * instr->i[7]);
            }

            // Envelope
            e = 1.f;
            if (j < attack) {
                e = (float)(j) / attack;
            }
            else if (j >= attack + sustain) {
                e -= ((float)(j)- attack - sustain) * releaseInv;
            }

            // Oscillator 1
            t = o1t;
            if (o1xenv > 0.0f) {
                t *= e * e;
            }
            c1 += t;
            rsample = osc1(c1) * o1vol;

            // Oscillator 2
            t = o2t;
            if (o2xenv > 0.0f) {
                t *= e * e;
            }
            c2 += t;
            rsample += osc2(c2) * o2vol;

            // Noise oscillator
            if (noiseVol > 0.0f) {
                rsample += (2.f * randfloat4k() - 1.f) * noiseVol;
            }

            // Add to (mono) channel buffer
            noteBuf[j] = (80.f * rsample * e);
        }

        return;
    };


    //--------------------------------------------------------------------------
    // Private members
    //--------------------------------------------------------------------------



    // Private variables set up by init()
    static sndbox_song* mSong;
    static unsigned int mLastRow, mCurrentCol, mNumWords;
    static float* mMixBuf;
    static unsigned int mMixBufLength = 0;


    //--------------------------------------------------------------------------
    // Initialization
    //--------------------------------------------------------------------------

    void sndbox_init(sndbox_song* song) {
        // Define the song
        mSong = song;

        // Init iteration state variables
        mLastRow = song->endPattern;
        mCurrentCol = 0;

        // Prepare song info
        mNumWords = song->rowLen * song->patternLen * (mLastRow + 1) * 2;

        // Create work buffer (initially cleared)
        mMixBuf = (float*)malloc(mNumWords * sizeof(float));
        memset(mMixBuf, 0, mNumWords * sizeof(float));
        mMixBufLength = mNumWords;
    }


    //--------------------------------------------------------------------------
    // Public methods
    //--------------------------------------------------------------------------

    // Generate audio data for a single track
    float sndbox_generate() {
        // Local variables
        int i, j, p, row, col, n, cp, k, rowStartSample;
        float b, t, lfor, e, x, rsample, f, da;

        // Put performance critical items in local variables
        float* chnBuf = malloc(mNumWords * sizeof(float));
        memset(chnBuf, 0, mNumWords * sizeof(float));
        sndbox_instrument* instr = &mSong->songData[mCurrentCol];
        unsigned int rowLen = mSong->rowLen;
        unsigned int patternLen = mSong->patternLen;

        // Clear effect state
        float low = 0.f, band = 0.f, high = 0.f;
        float lsample = 0.0f;
        int filterActive = 0;

        // Clear note cache.
        //var noteCache = [];

        // Patterns
        for (p = 0; p <= mLastRow; ++p) {
            cp = instr->p[p];

            // Pattern rows
            for (row = 0; row < patternLen; ++row) {
                // Execute effect command.
                auto cmdNo = cp ? instr->c[cp - 1].f[row] : 0;
                if (cmdNo) {
                    instr->i[cmdNo - 1] = instr->c[cp - 1].f[row + patternLen];

                    // Clear the note cache since the instrument has changed.
                    /*if (cmdNo < 16) {
                        noteCache = [];
                    }*/
                }

                // Put performance critical instrument properties in local variables
                float (*oscLFO)(float) = mOscillators[(int)instr->i[15]];
                float lfoAmt = instr->i[16] / 512,
                    lfoFreq = powf(2.f, instr->i[17] - 9.f) / rowLen,
                    fxLFO = instr->i[18],
                    fxFilter = instr->i[19],
                    fxFreq = instr->i[20] * 43.23529 * 3.141592 / 44100,
                    q = 1 - instr->i[21] / 255,
                    dist = instr->i[22] * 1e-5,
                    drive = instr->i[23] / 32,
                    panAmt = instr->i[24] / 512,
                    panFreq = 6.283184 * powf(2, instr->i[25] - 9) / rowLen,
                    dlyAmt = instr->i[26] / 255;
                int dly = (int)(instr->i[27] * rowLen) & ~1;  // Must be an even number

                // Calculate start sample number for this row in the pattern
                rowStartSample = (p * patternLen + row) * rowLen;

                // Generate notes for this pattern row
                for (col = 0; col < 4; ++col) {
                    n = cp ? instr->c[cp - 1].n[row + col * patternLen] : 0;
                    if (n) {
                        /*if (!noteCache[n]) {
                            noteCache[n] = createNote(instr, n, rowLen);
                        }*/
                        
                        int size = sndbox_getNoteSize(instr);
                        float* noteBuf = malloc(sizeof(float) * size);
                        sndbox_createNote(noteBuf, instr, n, rowLen);

                        for (j = 0, i = rowStartSample * 2; j < size; j++, i += 2) {
                            chnBuf[i] += noteBuf[j];
                        }
                        free(noteBuf);
                    }
                }

                // Perform effects for this pattern row
                for (j = 0; j < rowLen; j++) {
                    // Dry mono-sample
                    k = (rowStartSample + j) * 2;
                    rsample = chnBuf[k];

                    // We only do effects if we have some sound input
                    if (fabsf(rsample) > 0.0f || filterActive) {
                        // State variable filter
                        f = fxFreq;
                        if (fxLFO) {
                            f *= oscLFO(lfoFreq * k) * lfoAmt + 0.5f;
                        }
                        f = 1.5 * sinf(f);
                        low += f * band;
                        high = q * (rsample - band) - low;
                        band += f * high;
                        rsample = fxFilter == 3 ? band : fxFilter == 1 ? high : low;

                        // Distortion
                        if (dist) {
                            rsample *= dist;
                            rsample = rsample < 1 ? rsample > -1 ? osc_sin(rsample * .25) : -1 : 1;
                            rsample /= dist;
                        }

                        // Drive
                        rsample *= drive;

                        // Is the filter active (i.e. still audiable)?
                        filterActive = rsample * rsample > 1e-5f;

                        // Panning
                        t = sinf(panFreq * k) * panAmt + 0.5f;
                        lsample = rsample * (1.f - t);
                        rsample *= t;
                    }
                    else {
                        lsample = 0.f;
                    }

                    // Delay is always done, since it does not need sound input
                    if (k >= dly) {
                        // Left channel = left + right[-p] * t
                        lsample += chnBuf[k - dly + 1] * dlyAmt;

                        // Right channel = right + left[-p] * t
                        rsample += chnBuf[k - dly] * dlyAmt;
                    }

                    // Store in stereo channel buffer (needed for the delay effect)
                    chnBuf[k] = lsample;
                    chnBuf[k + 1] = rsample;

                    // ...and add to stereo mix buffer
                    mMixBuf[k] += lsample;
                    mMixBuf[k + 1] += rsample;
                }
            }
        }

        free(chnBuf);

        // Next iteration. Return progress (1.0 == done!).
        mCurrentCol++;
        return (float)mCurrentCol / (float)mSong->numChannels;
    };

    // Create a WAVE formatted Uint8Array from the generated audio data (returns number of bytes needed)
    unsigned int sndbox_createWave(short* wave, int withHdr) {
        // Create WAVE header
        unsigned int headerLen = 44;
        unsigned int l1 = headerLen + mNumWords * 2 - 8;
        unsigned int l2 = l1 - 36;
        if (!withHdr) headerLen = 0;
        if (wave == 0) return headerLen + mNumWords * 2;

        if (withHdr)
        {
            const char hdr[] = { 82, 73, 70, 70,
                l1 & 255, (l1 >> 8) & 255, (l1 >> 16) & 255, (l1 >> 24) & 255,
                87, 65, 86, 69, 102, 109, 116, 32, 16, 0, 0, 0, 1, 0, 2, 0,
                68, 172, 0, 0, 16, 177, 2, 0, 4, 0, 16, 0, 100, 97, 116, 97,
                l2 & 255, (l2 >> 8) & 255, (l2 >> 16) & 255, (l2 >> 24) & 255 };

            memcpy(wave, hdr, headerLen);
        }

        // Append actual wave data
        for (int i = 0, idx = headerLen/2; i < mNumWords; ++i) {
            // Note: We clamp here
            float y = mMixBuf[i];
            y = y < -32767 ? -32767 : (y > 32767 ? 32767 : y);
            wave[idx++] = (short)y;
        }

        // Return the WAVE formatted typed array
        return headerLen + mNumWords * 2;
    };

    // Get n samples of wave data at time t [s]. Wave data in range [-2,2].
    void sndbox_getData(float* d, double t, unsigned int n)
    {
        unsigned int i = 2 * (unsigned int)floor(t * 44100.0);
        for (unsigned int j = 0; j < 2 * n; j += 1) {
            unsigned int k = i + j;
            d[j] = t > 0 && k < mMixBufLength ? mMixBuf[k] / 32768 : 0;
        }
    }
