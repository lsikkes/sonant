#include <string.h>
#include <stdlib.h>
#include <math.h>

#include "soundbox.h"

#define ASSERT(x) if(!(x)) __debugbreak();
//#define ASSERT(x)

    //--------------------------------------------------------------------------
    // Private methods
    //--------------------------------------------------------------------------

    // Oscillators
static float osc_sin(float value) {
    return sinf(value * 6.283184);
}

static float osc_saw(float value) {
    return 2.0f * fmodf(value, 1.0f) - 1.0f;
}

static float osc_square(float value) {
    return fmodf(value, 1.f) < 0.5f ? 1.f : -1.f;
};

static float osc_tri(float value) {
    float v2 = fmodf(value, 1.0f) * 4.f;
    if (v2 < 2.f) return v2 - 1.f;
    return 3.f - v2;
};

// Array of oscillator functions
static float (*mOscillators[])(float) = {
    osc_sin,
    osc_square,
    osc_saw,
    osc_tri
};

static float getnotefreq(float fdiv, float n) {
    // 174.61.. / 44100 = 0.003959503758 (F3)
    return ((174.614f / 44100.f) * fdiv) * powf(2.f, (n - 128.f) / 12);
};

static unsigned int randseed = 1;
static float randfloat4k() {
    return (float)((randseed *= 0x15a4e35) % 255) / 255.0f;
}

unsigned int sndbox_getNoteSize(sndbox_instrument* instr)
{
    int attack = (instr->i[10] * instr->i[10]) * 4,
        sustain = (instr->i[11] * instr->i[11]) * 4,
        release = (instr->i[12] * instr->i[12]) * 4;
    return attack + sustain + release;
}


void sndbox_initNote(sndbox_noteState* note, sndbox_instrument* instr, float n, float rowLen, float freqDivider)
{
    note->j = 0;
    note->j2 = 0;
    note->o1t = 0.0f;
    note->o2t = 0.0f;
    note->c1 = 0.0f;
    note->c2 = 0.0f;
    note->n = n;
    note->fdiv = freqDivider;
    note->attack = (instr->i[10] * instr->i[10]) * 4.0f / note->fdiv;
    note->sustain = (instr->i[11] * instr->i[11]) * 4.0f / note->fdiv;
    note->release = (instr->i[12] * instr->i[12]) * 4.0f / note->fdiv;
    note->maxLen = sndbox_getNoteSize(instr) / note->fdiv;
    note->osc1 = mOscillators[(int)instr->i[0]],
        note->osc2 = mOscillators[(int)instr->i[4]];


    note->o1vol = instr->i[1];
    note->o1xenv = instr->i[3];
    note->o2vol = instr->i[5];
    note->o2xenv = instr->i[8];
    note->noiseVol = instr->i[9];
    note->releaseInv = 1.f / note->release;
    note->arpInterval = rowLen * powf(2.f, 2.f - instr->i[14]);
    note->instr_i2 = instr->i[2];
    note->instr_i6 = instr->i[6];
    note->instr_i7 = instr->i[7];
    note->arp = (int)instr->i[13];
}

int sndbox_generateNote(sndbox_noteState* note, float* noteBuf, int samples) {

    // Re-trig oscillators
    int out = 0;

    // Generate one note (attack + sustain + release)
    for (; note->j < note->maxLen && out < samples; note->j++, (note->j2++)) {

        if (note->j2 >= 0) {
            // Switch arpeggio note.
            note->arp = (note->arp >> 8) | ((note->arp & 255) << 4);
            note->j2 -= note->arpInterval;

            // Calculate note frequencies for the oscillators
            note->o1t = getnotefreq(note->fdiv, note->n + (note->arp & 15) + note->instr_i2 - 128.f);
            note->o2t = getnotefreq(note->fdiv, note->n + (note->arp & 15) + note->instr_i6 - 128.f) * (1.f + 0.0008 * note->instr_i7);
        }

        // Envelope
        float e = 1.f;
        if (note->j < note->attack) {
            e = (float)(note->j) / note->attack;
        }
        else if (note->j >= note->attack + note->sustain) {
            e -= ((float)(note->j) - note->attack - note->sustain) * note->releaseInv;
        }

        // Oscillator 1
        float t = note->o1t;
        if (note->o1xenv > 0.0f) {
            t *= e * e;
        }
        note->c1 += t;
        float rsample = note->osc1(note->c1) * note->o1vol;

        // Oscillator 2
        t = note->o2t;
        if (note->o2xenv > 0.0f) {
            t *= e * e;
        }
        note->c2 += t;
        rsample += note->osc2(note->c2) * note->o2vol;

        // Noise oscillator
        if (note->noiseVol > 0.0f) {
            rsample += (2.f * randfloat4k() - 1.f) * note->noiseVol;
        }

        // Add to (mono) channel buffer
        noteBuf[out++] = (80.f * rsample * e);
    }

    return out;
};


//--------------------------------------------------------------------------
// Private members
//--------------------------------------------------------------------------

int sndbox_noteIsDone(sndbox_noteState* note) { return note->j == note->maxLen; }

// Private variables set up by init()

//--------------------------------------------------------------------------
// Public methods
//--------------------------------------------------------------------------

int sndbox_getMaxDelay(sndbox_chnstate* chnl)
{
    sndbox_instrument* instr = &chnl->mSong->songData[chnl->currentChnl];
    float instr27 = instr->i[27];
    float instr26 = instr->i[26];
    unsigned int rowLen = chnl->mSong->rowLen;

    // Check if any part of the song increases delay buf
    unsigned int patternLen = chnl->mSong->patternLen;
    int p, row;
    for (p = 0; p <= chnl->mLastRow; ++p) {
        char cp = instr->p[p];
        for (row = 0; row < patternLen; ++row) {
            // Update delay to max value
            int cmdNo = cp ? instr->c[cp - 1].f[row] : 0;
            if (cmdNo == 28) {
                if (instr->c[cp - 1].f[row + patternLen] > instr27)
                    instr27 = instr->c[cp - 1].f[row + patternLen];
            }
            if (cmdNo == 27) {
                if (instr->c[cp - 1].f[row + patternLen] > instr26)
                    instr26 = instr->c[cp - 1].f[row + patternLen];
            }
        }
    }

    return (int)(instr27 * rowLen) & ~1;  // Must be an even number
}

void sndbox_init_generate_chn(sndbox_song* song, sndbox_config* config, sndbox_chnstate* state, int chn)
{
    memset(state, 0, sizeof(sndbox_chnstate));
    state->freqDivider = config->freqDivider;
    state->channels = config->channels;

    state->mSong = song;
    state->mLastRow = song->endPattern;
    state->mNumWords = song->rowLen * song->patternLen * (state->mLastRow + 1) * state->channels;

    state->currentChnl = chn;
    state->instr = &state->mSong->songData[state->currentChnl];
    state->rowLen = state->mSong->rowLen / state->freqDivider;
    state->patternLen = state->mSong->patternLen;
    state->done = 0;

    // Delay state
    state->delayMax = sndbox_getMaxDelay(state);
    state->delayMaxFloats = 2 * state->delayMax;
    state->delayBuf = calloc(state->delayMaxFloats, sizeof(float));
    state->delayOffset = 0;

    // Clear effect state
    state->low = 0.f, state->band = 0.f, state->high = 0.f;
    state->filterActive = 0;
}

// Generate audio data for a single track
int sndbox_generate(sndbox_chnstate* st, float* mMixBuf, int* samplesWritten) {

    int localK = 0;
    *samplesWritten = 0;

    if (st->done == 1)
        return 1;

    // Delay system
    #define WRITE_INTO_DELAY(x,y)   if(st->delayOffset + 2 >= st->delayMaxFloats ) st->delayOffset = 0; ASSERT(st->delayOffset + 1 < st->delayMaxFloats); st->delayBuf[st->delayOffset] = x; st->delayBuf[st->delayOffset+1] = y; st->delayOffset+= 2; 
    #define READ_DELAY(offset)      int _offs = st->delayOffset - offset; if(_offs < 0) _offs += st->delayMaxFloats; ASSERT(_offs + 1 < st->delayMaxFloats);  float delay_x = st->delayBuf[_offs]; float delay_y = st->delayBuf[_offs+1]; 

    // Note system
    #define FIND_EMPTY_NOTE()       int empty_note = -1; \
                                            for(int _i=0; _i < MAX_NOTES; _i++) { if(sndbox_noteIsDone(st->notes+_i)) { empty_note = _i; break; } }

    ASSERT(isnan(st->band) == 0);

    // Patterns
    while (st->p <= st->mLastRow) {
        st->cp = st->instr->p[st->p];

        // Pattern rows
        while (st->row < st->patternLen) {

            // Execute effect command.
            int cmdNo = st->cp ? st->instr->c[st->cp - 1].f[st->row] : 0;
            if (cmdNo) {
                st->instr->i[cmdNo - 1] = st->instr->c[st->cp - 1].f[st->row + st->patternLen];
            }

            // -- SEQUENCER

            // Calculate start sample number for this row in the pattern
            int rowStartSampleOut = (st->p * st->patternLen + st->row) * st->rowLen;

            if (st->cp > 0)
            {
                // Generate notes for this pattern row
                for (st->col = 0; st->col < 4; ++st->col) {
                    int n = st->instr->c[st->cp - 1].n[st->row + st->col * st->patternLen];
                    if (n) {
                        // stream samples so that we don't need a huge memory buffer
                        FIND_EMPTY_NOTE();
                        ASSERT(empty_note != -1);
                        if (empty_note + 1 > st->active_notes)
                            st->active_notes = empty_note + 1;
                        sndbox_noteState* note = st->notes + empty_note;
                        sndbox_initNote(note, st->instr, n, st->rowLen, st->freqDivider);
                    }
                }
            }

            // Generate note data
            float rowBuf[40000];
            memset(rowBuf, 0, st->rowLen * sizeof(float));
            int isFirst = 1;
            for (st->i = 0; st->i < st->active_notes; st->i++)
            {
                if (!sndbox_noteIsDone(st->notes + st->i))
                {
                    int smp;
                    if (isFirst == 1)
                    {
                        smp = sndbox_generateNote(st->notes + st->i, rowBuf, st->rowLen);
                        isFirst = 0;
                    }
                    else
                    {
                        smp = sndbox_generateNote(st->notes + st->i, rowBuf + st->rowLen, st->rowLen);
                        for (st->j = 0; st->j < smp; st->j++)
                            rowBuf[st->j] += rowBuf[st->j + st->rowLen];
                    }
                }
            }

            // -- EFFECTS -- 
            // Put performance critical instrument properties in local variables
            float (*oscLFO)(float) = mOscillators[(int)st->instr->i[15]];
            float lfoAmt = st->instr->i[16] / 512,
                lfoFreq = powf(2.f, st->instr->i[17] - 9.f) / st->rowLen,
                fxLFO = st->instr->i[18],
                fxFilter = st->instr->i[19],
                fxFreq = st->instr->i[20] * 43.23529 * 3.141592 / (44100 / st->freqDivider),
                q = 1 - st->instr->i[21] / 255,
                dist = st->instr->i[22] * 1e-5,
                drive = st->instr->i[23] / 32,
                panAmt = st->instr->i[24] / 512,
                panFreq = 6.283184 * powf(2, st->instr->i[25] - 9) / st->rowLen,
                dlyAmt = st->instr->i[26] / 255;
            int dly = (int)(st->instr->i[27] * st->rowLen) & ~1;  // Must be an even number

            // Perform effects for this pattern row
            for (st->j = 0; st->j < st->rowLen; st->j++) {
                // Dry mono-sample
                st->k = (rowStartSampleOut + st->j) * st->channels;
                float rsample = rowBuf[st->j];
                float lsample = 0.0f;

                // We only do effects if we have some sound input
                if (fabsf(rsample) > 1e-5f || st->filterActive) {
                    // State variable filter
                    st->f = fxFreq;
                    if (fxLFO > 0.0f) {
                        st->f *= oscLFO(lfoFreq * st->k) * lfoAmt + 0.5f;
                    }
                    st->f = 1.5 * sinf(st->f);

                    // threshold st->f, otherwise values will explode
                    if (st->f > 1.20f)
                        st->f = 1.20f;
                    
                    st->low += st->f * st->band;
                    ASSERT(isfinite(st->low));
                    st->high = q * (rsample - st->band) - st->low;
                    ASSERT(isfinite(st->high));
                    st->band += st->f * st->high;
                    ASSERT(isfinite(st->band));
                    
                    rsample = fxFilter == 3 ? st->band : fxFilter == 1 ? st->high : st->low;

                    // Distortion
                    if (dist > 0.0f) {
                        rsample *= dist;
                        rsample = rsample < 1 ? rsample > -1 ? osc_sin(rsample * .25) : -1 : 1;
                        rsample /= dist;
                    }

                    // Drive
                    rsample *= drive;

                    // Is the filter active (i.e. still audiable)?
                    st->filterActive = rsample * rsample > 1e-5f;

                    if (st->channels == 2)
                    {
                        // Panning
                        st->t = sinf(panFreq * st->k) * panAmt + 0.5f;
                        lsample = rsample * (1.f - st->t);
                        rsample *= st->t;
                    }
                }


                // -- DELAY EFFECT
                if (dly > 0 && dlyAmt > 0.001f)
                {
                    // Delay is alwa6ys done, since it does not need sound input
                    READ_DELAY(dly); // read delay and store in delay_x and delay_y

                    // Left channel = left + right[-p] * t
                    if (st->channels == 2)
                        lsample += delay_y * dlyAmt;

                    // Right channel = right + left[-p] * t
                    rsample += delay_x * dlyAmt;

                    // Remember new samples for delay buffer
                    WRITE_INTO_DELAY(lsample, rsample);
                }
                // -- END DELAY

                // ...and add to stereo mix buffer
                localK = st->j * st->channels;
                if (st->channels == 2)
                {
                    mMixBuf[localK] += lsample;
                    mMixBuf[localK + 1] += rsample;
                }
                else
                {
                    mMixBuf[localK] += rsample;
                }
            }

            *samplesWritten = st->rowLen * st->channels;
            ++st->row;
            return 0;
        }
        st->row = 0;
        ++st->p;
    }

    free(st->delayBuf);
    st->delayBuf = 0;
    st->done = 1;
    return 1;
};

// Create a WAVE formatted Uint8Array from the generated audio data (returns number of bytes needed)
unsigned int sndbox_createWave(short* wave, int withHdr, float* mMixBuf, int numSamplesMulChn) {
    // Create WAVE header
    unsigned int headerLen = 44;
    unsigned int l1 = headerLen + numSamplesMulChn * sizeof(short) - 8;
    unsigned int l2 = l1 - 36;
    if (!withHdr) headerLen = 0;
    if (wave == 0) return headerLen + numSamplesMulChn * sizeof(short);

    if (withHdr)
    {
        const char hdr[] = { 82, 73, 70, 70,
            l1 & 255, (l1 >> 8) & 255, (l1 >> 16) & 255, (l1 >> 24) & 255,
            87, 65, 86, 69, 102, 109, 116, 32, 16, 0, 0, 0, 1, 0, 2, 0,
            68, 172, 0, 0, 16, 177, 2, 0, 4, 0, 16, 0, 100, 97, 116, 97,
            l2 & 255, (l2 >> 8) & 255, (l2 >> 16) & 255, (l2 >> 24) & 255 };

        memcpy(wave, hdr, headerLen);
    }

    // Append actual wave data
    for (int i = 0, idx = headerLen / 2; i < numSamplesMulChn; ++i) {
        // Note: We clamp here
        float y = mMixBuf[i];
        y = y < -32767 ? -32767 : (y > 32767 ? 32767 : y);
        wave[idx++] = (short)y;
    }

    // Return the WAVE formatted typed array
    return headerLen + numSamplesMulChn * sizeof(short);
};