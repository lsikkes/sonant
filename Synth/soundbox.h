#pragma once

typedef struct {
    // Notes
    unsigned char   n[128];          // Notes (pattern length is 32, 4 parallel allowed)
    // Modifiers
    unsigned char   f[64];
} sndbox_column;

// Instrument
typedef struct {

    /* // INPUTS:
        //  0 OSC1_WAVEFORM
        //  1 OSC1_VOL
        //  2 OSC1_SEMI
        //  3 OSC1_XENV
        //  4 OSC2_WAVEFORM
        //  5 OSC2_VOL
        //  6 OSC2_SEMI
        //  7 OSC2_DETUNE
        //  8 OSC2_XENV
        //  9 NOISE_VOL
        // 10 ENV_ATTACK
        // 11 ENV_SUSTAIN
        // 12 ENV_RELEASE
        // 13 ARP_CHORD
        // 14 ARP_SPEED
        // 15 LFO_WAVEFORM
        // 16 LFO_AMT
        // 17 LFO_FREQ
        // 18 LFO_FX_FREQ
        // 19 FX_FILTER
        // 20 FX_FREQ
        // 21 FX_RESONANCE
        // 22 FX_DIST
        // 23 FX_DRIVE
        // 24 FX_PAN_AMT
        // 25 FX_PAN_FREQ
        // 26 FX_DELAY_AMT
        // 27 FX_DELAY_TIME
    */

    // Inputs
    float i[28];            // Inputs ( see above )

    // Patterns
    char   p[500];          // Pattern order (Maximum 500 patterns)
// Columns
    sndbox_column          c[10];          // Columns (10 maximum)
} sndbox_instrument;

// Songs
typedef struct {
    char ident[4];
    // Instruments
    sndbox_instrument      songData[16];           // Instruments (16 maximum)

    unsigned int rowLen;
    unsigned int patternLen;
    unsigned int endPattern;
    unsigned int numChannels;
} sndbox_song;

//void sndbox_init(sndbox_song* song);
//float sndbox_generate();
//unsigned int sndbox_createWave(short* wave, int withHdr); // returns size in bytes
unsigned int sndbox_getNoteSize(sndbox_instrument* instr); // in samples
//void sndbox_generateNote(float* noteBuf, sndbox_instrument* instr, float n, float rowLen);


// STREAMING VERSION

typedef struct
{
    int channels;
    float freqDivider;

}sndbox_config;

typedef struct
{
    int j, j2, arp;
    float o1t, o2t;
    float c1, c2;
    float n;
    int maxLen;
    float attack, sustain, release;
    float o1vol, o1xenv, o2vol, o2xenv, noiseVol, releaseInv, arpInterval;
    float instr_i2, instr_i6, instr_i7;
    float fdiv;
    float (*osc1)(float);
    float (*osc2)(float);
} sndbox_noteState;

int sndbox_noteIsDone(sndbox_noteState* note);
void sndbox_initNote(sndbox_noteState* note, sndbox_instrument* instr, float n, float rowLen, float freqDivider);
int sndbox_generateNote(sndbox_noteState* note, float* noteBuf, int samples);

#define MAX_NOTES 32
typedef struct
{
    // song state
    sndbox_song* mSong;
    unsigned int mLastRow, mNumWords;
    unsigned int rowLen, patternLen, currentChnl;
    float freqDivider;
    int channels;

    // vars
    int i, j, p, row, col, cp, k, done;
    float t, f;

    // instrument
    sndbox_instrument* instr;

    // filters
    int bufferSpace;
    float low, band, high;
    int filterActive;
    // notes
    int active_notes;
    sndbox_noteState notes[MAX_NOTES];
    // delay
    float* delayBuf;
    int delayMax, delayMaxFloats, delayOffset;
} sndbox_chnstate;
void sndbox_init_generate_chn(sndbox_song* song, sndbox_config* config, sndbox_chnstate* state, int chn);
int sndbox_generate(sndbox_chnstate* state, float* mMixBuf, int* samplesWritten);
unsigned int sndbox_createWave(short* wave, int withHdr, float* mMixBuf, int numSamplesMulChn);